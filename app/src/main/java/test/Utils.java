package test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class Utils {
    
    public static final String cheminRapportsVISA = "\\Maïa Sonnier\\IF2304-SGP Pont de Sèvres Lot 1 - Documents\\13 00 Documents d'exécution\\13 09 Note d'obs. MOE - Fiches VISA\\Rapports Etats des VISAS";
    public static final String cheminretoursVISA = "\\Maïa Sonnier\\IF2304-SGP Pont de Sèvres Lot 1 - Documents\\13 00 Documents d'exécution\\13 09 Note d'obs. MOE - Fiches VISA\\0-Retour VISAS";

    public static WebDriver getDriver(Boolean pourVisa){


        System.setProperty("webdriver.chrome.driver", System.getProperty("user.home") + cheminRapportsVISA + "\\x-ressources\\chromedriver.exe");
        //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/app/src/main/x-resources/chromedriver.exe");


        ChromeOptions options = new ChromeOptions();
        Map<String, Object> chromePrefs = new HashMap<>();

        if(pourVisa){
            chromePrefs.put("download.default_directory", System.getProperty("user.home")+cheminretoursVISA+"\\tmp");
        }else{
            chromePrefs.put("download.default_directory", System.getProperty("user.home")+cheminRapportsVISA);
        }

        
        chromePrefs.put("plugins.always_open_pdf_externally", true);
        options.setExperimentalOption("prefs", chromePrefs);
        //options.addArguments("headless");

        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().maximize();

        return driver;
    }

    public static WebDriver getDriver(String downloadFolderPath){
        //System.setProperty("webdriver.chrome.driver", System.getProperty("user.home") + cheminDossierVISA + "\\x-ressources\\chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", downloadFolderPath);

        ChromeOptions options = new ChromeOptions();
        Map<String, Object> chromePrefs = new HashMap<>();

        chromePrefs.put("download.default_directory", System.getProperty("user.home")+cheminRapportsVISA);
        chromePrefs.put("plugins.always_open_pdf_externally", true);
        options.setExperimentalOption("prefs", chromePrefs);

        
        WebDriver driver = new ChromeDriver(options);
        driver.manage().window().setSize(new Dimension(10, 10));

        return driver;
    }

    public static void majJournalDl(String code){
        File file = new File(System.getProperty("user.home")+cheminRapportsVISA+"\\journal.txt");
        try {
            FileUtils.writeStringToFile(file, code+"\n", Charset.defaultCharset(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
