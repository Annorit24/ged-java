package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Ged {
    
    private final String usernameString;
    private final String passwordString;

    public Ged(String usernaString, String passwoString){
        this.usernameString = usernaString;
        this.passwordString = passwoString;
    }


    public void connexionGed(WebDriver driver){
        driver.get("https://ged-gdc.societedugrandparis.fr/LascomPLM/Login.aspx?ReturnUrl=%2fLascomPLM%2fSGP_DOC_PROD");
        
        //Connexion à la GED
        WebElement username = driver.findElement(By.id("ContentPlaceHolderMiddle_userNameTextBox"));
        WebElement password = driver.findElement(By.id("ContentPlaceHolderMiddle_passwordTextBox"));
        WebElement submitBtn = driver.findElement(By.id("ContentPlaceHolderMiddle_loginButton"));

        username.sendKeys(usernameString);
        password.sendKeys(passwordString);
        submitBtn.click();

        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
    }

}
