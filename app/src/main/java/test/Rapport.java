package test;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Rapport {
    
    private final String[] args;

    public Rapport(String[] args){
        this.args = args;
    }

    public void recuperationRapport(){
         //Récuperation du driver
        WebDriver driver = Utils.getDriver(false);

        //Connexion à la GED
        Ged ged = new Ged(args[0], args[1]);
        ged.connexionGed(driver);
        
        //Aller dans tous les processus
        driver.get("https://ged-gdc.societedugrandparis.fr/LascomPLM/SGP_DOC_PROD/Compat/Assigned.asp?MenuItem=All&FilterId=&IsPublic=");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        //Aller dans le premier processus de la liste
        WebElement frame1 = driver.findElement(By.id("pageContent_compatFrame"));
        driver.switchTo().frame(frame1);
        WebElement frame2 = driver.findElement(By.id("iframeMain"));
        driver.switchTo().frame(frame2);
        WebElement abtn = driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/div/table/tbody/tr/td[1]/div[2]/table/tbody/tr/td[3]/a"));
        abtn.click();

        //Aller dans Travail
        WebElement travail = driver.findElement(By.xpath("/html/body/nav/a[3]"));
        travail.click();

        //Relancer le Travail
        WebElement frame3 = driver.findElement(By.id("PackageFormFrame"));
        driver.switchTo().frame(frame3);
        WebElement relancer = driver.findElement(By.xpath("/html/body/form/div[3]/table/tbody/tr[1]/td/span/div[1]/div[2]/button[2]"));
        relancer.click();

        //Retour à la liste des processus
        driver.switchTo().defaultContent();
        driver.get("https://ged-gdc.societedugrandparis.fr/LascomPLM/SGP_DOC_PROD/Compat/Assigned.asp?MenuItem=All&FilterId=&IsPublic=");

        //Aller dans l'autre rapport
        driver.switchTo().defaultContent();
        WebElement frame4 = driver.findElement(By.id("pageContent_compatFrame"));
        driver.switchTo().frame(frame4);
        WebElement frame5 = driver.findElement(By.id("iframeMain"));
        driver.switchTo().frame(frame5);
        WebElement abtn1 = driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/div/table/tbody/tr/td[1]/div[2]/table/tbody/tr/td[3]/a"));
        abtn1.click();

        //Aller dans Travail
        WebElement travail2 = driver.findElement(By.xpath("/html/body/nav/a[3]"));
        travail2.click();
            
        //Relancer le Travail
        WebElement frame6 = driver.findElement(By.id("PackageFormFrame"));
        driver.switchTo().frame(frame6);
        WebElement relancer2 = driver.findElement(By.xpath("/html/body/form/div[3]/table/tbody/tr[1]/td/span/div[1]/div[2]/button[2]"));
        relancer2.click();

        //Retour à la liste des processus
        driver.switchTo().defaultContent();
        driver.get("https://ged-gdc.societedugrandparis.fr/LascomPLM/SGP_DOC_PROD/Compat/Assigned.asp?MenuItem=All&FilterId=&IsPublic=");


        boolean wait = true;

        while(wait){

            try{
                //Aller dans le deuxième processus de la liste
                WebElement frame7 = driver.findElement(By.id("pageContent_compatFrame"));
                driver.switchTo().frame(frame7);
                WebElement frame8 = driver.findElement(By.id("iframeMain"));
                driver.switchTo().frame(frame8);
                WebElement abtn3 = driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/div/table/tbody/tr/td[1]/div[2]/table/tbody/tr[2]/td[3]/a"));
                abtn3.click();

                wait = false;
            }catch(Exception ignore) {
                System.out.println("err");
                try {
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                driver.navigate().refresh();
                //Continue
            }

        }



        WebElement frame9 = driver.findElement(By.id("PackageFormFrame"));
        driver.switchTo().frame(frame9);

        WebElement download = driver.findElement(By.xpath("/html/body/form/div[3]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[5]/td/a"));
        download.click();

        //Retour à la liste des processus
        driver.switchTo().defaultContent();
        driver.get("https://ged-gdc.societedugrandparis.fr/LascomPLM/SGP_DOC_PROD/Compat/Assigned.asp?MenuItem=All&FilterId=&IsPublic=");


        //Aller dans le premier processus de la liste
        WebElement frame10 = driver.findElement(By.id("pageContent_compatFrame"));
        driver.switchTo().frame(frame10);
        WebElement frame11 = driver.findElement(By.id("iframeMain"));
        driver.switchTo().frame(frame11);
        WebElement abtn4 = driver.findElement(By.xpath("/html/body/table/tbody/tr/td/table[2]/tbody/tr/td/div/table/tbody/tr/td[1]/div[2]/table/tbody/tr[1]/td[3]/a"));
        abtn4.click();


        WebElement frame12 = driver.findElement(By.id("PackageFormFrame"));
        driver.switchTo().frame(frame12);

        WebElement download2 = driver.findElement(By.xpath("/html/body/form/div[3]/table/tbody/tr[2]/td/table/tbody/tr[1]/td/table/tbody/tr[5]/td/a"));
        download2.click();

        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.close();
        driver.quit();
    }

}
