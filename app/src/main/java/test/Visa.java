package test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Visa {
    
    private final String[] args;

    public Visa(String[] args){
        this.args = args;
    }

    public void recuperationVisa(List<String> codeGedDocuments){
        //Récuperation du driver
        WebDriver driver = Utils.getDriver(true);

        //Connexion à la GED
        Ged ged = new Ged(args[0], args[1]);
        ged.connexionGed(driver);
        codeGedDocuments.forEach(t -> {

            recuperationVisa(t, driver);
            driver.get("https://ged-gdc.societedugrandparis.fr/LascomPLM/SGP_DOC_PROD");
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            Utils.majJournalDl(t);
        });

        driver.close();
    }

    private void recuperationVisa(String codeGedDocument, WebDriver driver){

        WebElement searchInput = driver.findElement(By.id("searchControl_searchTextBox"));
        searchInput.sendKeys("\"" + codeGedDocument + "\"");
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        WebElement searchButton = driver.findElement(By.className("plm-icon-sm"));
        searchButton.click();
        
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        List<WebElement> cards = driver.findElements(By.xpath("/html/body/form/div[6]/div/div[2]/div/div[2]/div[3]/div[1]/div/a"));
        System.out.println(cards.size());

        int attempt = 0;

        while(attempt <= 3){
            try{
                cards.get(0).click();
                break;  
            }catch(Exception e){
                attempt++;
            }
        }

        
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        WebElement pxEl = driver.findElement(By.xpath("/html/body/form/div[3]/div[1]/div[1]/a[2]"));
        String px = pxEl.getText();

        WebElement frame1 = driver.findElement(By.id("pageContent_compatFrame"));
        driver.switchTo().frame(frame1);
        WebElement frame2 = driver.findElement(By.id("iframeMain"));
        driver.switchTo().frame(frame2);

        

        WebElement codificationEl = driver.findElement(By.xpath("/html/body/form[1]/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/div/div[1]/table/tbody/tr[1]/td/div/div/table[1]/tbody/tr/td/span/div/div[2]/div/table/tbody/tr[1]/td/table/tbody/tr[2]/td[2]/input[1]"));
        WebElement titreEl = driver.findElement(By.xpath(       "/html/body/form[1]/table/tbody/tr[3]/td/div/table/tbody/tr[2]/td/div/div[1]/table/tbody/tr[1]/td/div/div/table[1]/tbody/tr/td/span/div/div[1]/div/table/tbody/tr[5]/td[2]/input[1]"));
        String codification = codificationEl.getAttribute("value");
        String titre = titreEl.getAttribute("value");

        

        try{
            Integer index = px.contains("Graphiques") ? 6 : 5;
            WebElement avisVisaBtn = driver.findElement(By.xpath("/html/body/form[1]/table/tbody/tr[2]/td/nav/a[" + index + "]"));
            avisVisaBtn.click();
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        }catch(Exception ignored ){
            return;
        }


        List<WebElement> listVisaBtn = driver.findElements(By.xpath("/html/body/div[3]/table/tbody/tr[2]/td/table/tbody/tr[4]/td/div/form/table/tbody/tr[2]/td/div/div/table/tbody/tr/td[1]/div[2]/div[2]/table/tbody/tr"));
        System.out.println("taille : "+listVisaBtn.size());


        for (int i = 1; i < listVisaBtn.size(); i++) {
            int j = i + 1;

            WebElement downloadBtn = driver.findElement(By.xpath("/html/body/div[3]/table/tbody/tr[2]/td/table/tbody/tr[4]/td/div/form/table/tbody/tr[2]/td/div/div/table/tbody/tr/td[1]/div[2]/div[2]/table/tbody/tr["+ j +"]/td[4]/a"));
            
            if(downloadBtn.getCssValue("visibility").equalsIgnoreCase("visible")){
                downloadBtn.click();
                driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                driver.switchTo().window(new ArrayList<>(driver.getWindowHandles()).get(1));

                driver.switchTo().defaultContent();
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }

                System.out.println("ICI");
                List<WebElement> docsATelecharger = driver.findElements(By.xpath("/html/body/div[3]/div[1]/form/div/ul/li/ul/li"));
                System.out.println(docsATelecharger.size());

                for(int k = 1; k <= docsATelecharger.size(); k++){
                    System.out.println("ENTER");
                    driver.switchTo().defaultContent();
                    WebElement docATelecharger = driver.findElement(By.xpath("/html/body/div[3]/div[1]/form/div/ul/li/ul/li["+k+"]/div/a"));
                    String nomDocATelecharger = docATelecharger.getText();
                    docATelecharger.click();

                    try{
                        driver.switchTo().defaultContent();
                        System.out.println("8");
                        WebElement frame3 = driver.findElement(By.id("bravaFrame"));
                        System.out.println("7");
                        driver.switchTo().frame(frame3);
                        System.out.println("6");
                        WebElement downloadPdfBtn = driver.findElement(By.xpath("/html/body/div/div/a"));
                        System.out.println("5");
                        downloadPdfBtn.click();
                        System.out.println("4");
                    }catch (Exception ignore){
                        System.out.println("3");
                        //System.out.println(ignore.getMessage());
                    }

                    try {
                        TimeUnit.SECONDS.sleep(3);
                    } catch (InterruptedException e) {
                        System.out.println("2");
                        System.out.println(e.getMessage());
                    }

                    System.out.println("1");
                    if(nomDocATelecharger.contains(".docx") || nomDocATelecharger.contains(".pdf")){
                        createDir(codification, titre);
                    }  
                }

                driver.close();
                driver.switchTo().window(new ArrayList<>(driver.getWindowHandles()).get(0));

            }

            driver.switchTo().defaultContent();
            WebElement frame4 = driver.findElement(By.id("pageContent_compatFrame"));
            driver.switchTo().frame(frame4);
            WebElement frame5 = driver.findElement(By.id("iframeMain"));
            driver.switchTo().frame(frame5);

            driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        }

    }

    private void createDir(String codification, String titre){
        File target = new File(System.getProperty("user.home") + Utils.cheminretoursVISA +"\\"+codification+"\\"+titre);
        File tmp = new File(System.getProperty("user.home") + Utils.cheminretoursVISA + "\\tmp\\");

        while(tmp.list().length == 0){
            //wait
        }

        if(tmp.listFiles().length == 0)return;

        File doc = tmp.listFiles()[0];

        try{
            FileUtils.moveFileToDirectory(doc, target,true);
            
        }catch(Exception ignore){
            System.out.println(ignore.getMessage());
        }
        


    }


}
